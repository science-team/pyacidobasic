#!/usr/bin/python3
# -*- coding: utf-8 -*-

from .Ui_curvecontrol import Ui_Form
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QColor
from PyQt6.QtWidgets import QWidget, QColorDialog

licence = {}
licence['en'] = """
    curveControl.py is part of the package pyacidobasic:

    a program to simulate acido-basic equilibria

    Copyright (C) 2010-2024 Georges Khaznadar <georgesk@debian.org>
    Copyright (C) 2024 philippe DALET <philippe.dalet@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class CurveControl(QWidget):
    def __init__(self, parent=None, mw=None, html="", color=QColor("black"),
                 checked=False, modifiable=True, courbe=None):
        """
        Le constructeur
        @param parent un widget parent
        @param mw un QMainWindow
        @param html un texte riche
        @param color une couleur
        @param checked une valeur booléenne
        @param modifiable une valeur booléenne
        @param courbe la courbe sous contrôle
        """
        QWidget.__init__(self, parent)
        self.mw = mw
        self.courbe = courbe
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.textEdit.setHtml(html)
        self.setColor(color)
        if checked:
            self.ui.checkBox.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.checkBox.setCheckState(Qt.CheckState.Unchecked)
        self.ui.checkBox.setEnabled(modifiable)
        self.modifiable = modifiable
        self.ui.checkBox.stateChanged.connect(self.coche)
        self.ui.toolButton.clicked.connect(self.couleur)

    def setColor(self, color):
        """
        colorie le bouton
        @param color la couleur à appliquer
        """
        self.color = QColor(color)
        self.ui.toolButton.setStyleSheet(
            "QPushButton { background-color: %s }"
            "QPushButton:pressed { background-color: %s }" % (
                self.color.name(), self.color.lighter(125).name()
            )
        )
        return

    def coche(self, state):
        """
        fonction de rappel de la case à cocher
        @param state l'état de la case
        """
        if state:
            self.courbe.show()
        else:
            self.courbe.hide()

    def couleur(self):
        """
        fonction de rappel du bouton de couleur
        """
        if not self.modifiable:
            return
        qd = QColorDialog(self.color)
        qd.exec_()
        color = qd.selectedColor()
        self.setColor(color)
        self.courbe.setPen(color)
        return

    def checkState(self):
        """
        @return l'état du bouton à cocher
        """
        return self.ui.checkBox.checkState()



