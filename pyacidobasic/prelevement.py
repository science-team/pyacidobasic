#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt6.QtCore import QLocale
from PyQt6.QtWidgets import QDialog, QMessageBox, QApplication
from PyQt6.QtGui import QDoubleValidator
from .Ui_prelevement import Ui_Dialog
from .mainwindow import locale
import sys

licence = """
    file prelevement.py: part of the package pyacidobasic version %s:

    Copyright (C) 2010-2013 Georges Khaznadar <georgesk@debian.org>
    Copyright (C) 2024 philippe DALET <philippe.dalet@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class prelevementDialog(QDialog):
    def __init__(self, produit="non défini"):
        """
        Le constructeur
        @param produit : le nom du produit prélevé
        """
        QDialog.__init__(self)
        self.locale = QLocale(locale)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        vC = QDoubleValidator(0, 50, 7, None)
        self.ui.lineEditC.setValidator(vC)
        self.ui.lineEditC.setText(self.locale.toString(0.1))
        vL = QDoubleValidator(0, 1000, 2, None)
        self.ui.lineEditV.setValidator(vL)
        self.ui.lineEditV.setText(self.locale.toString(10))
        self.ui.labelPrelevement.setText(f"prélèvement : {produit}")

    def getC(self):
        try:
            result = self.locale.toDouble(self.ui.lineEditC.text())[0]
        except ValueError:
            result = 0.0
        return result

    def getV(self):
        try:
            result = self.locale.toDouble(self.ui.lineEditV.text())[0]
        except ValueError:
            result = 0.0
        return result

def concentrationVolumePrelevement(produit):
    p = prelevementDialog(produit)
    accepte = False
    while not accepte:
        accepte = p.exec() and p.getC() != 0.0 and p.getV() != 0.0
        if not accepte:
            QMessageBox.information(p, "Attention", "Entrez le valeurs de \
                                    concentration et de volume !")
    return p.getC(), p.getV()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    c, v = concentrationVolumePrelevement("acide chlorhydrique")
    print(f"Concentration, volume : {c} , {v}")
    sys.exit(0)



