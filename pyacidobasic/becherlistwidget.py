#! /usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt6.QtCore import QEvent
from PyQt6.QtWidgets import QListWidget, QApplication
from .prelevement import concentrationVolumePrelevement

licence = """
    file becherlistwidget.py: part of the package pyacidobasic version %s:

    Copyright (C) 2010-2024 Georges Khaznadar <georgesk@debian.org>
    Copyright (C) 2024 philippe DALET <philippe.dalet@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class BecherListWidget(QListWidget):
    def __init__(self, parent):
        QListWidget.__init__(self, parent)
        self.contenu = []

    def dump(self):
        """
        @return renvoie un objet à enregistrer dans un fichier
        """
        return self.contenu

    def load(self, liste):
        """
        Récupère une donnée venant d'un fichier
        @param liste une liste d'acide-base
        """
        self.clear()
        self.contenu = liste
        for obj in liste:
            self.addItem(self.nouveauTexte(obj))
        # on envoie un signal pour dire qu'il faut tracer les courbes
        self.findMainWindow().simulRequest.emit()

    def load_from_json(self, obj):
        """
        Récupère une donnée venant d'un fichier json
        @param obj un objet obtenu par jsonpickle.decode()
        """
        self.clear()
        self.contenu = obj["becher"].contenu
        for ab in self.contenu:
            self.addItem(self.nouveauTexte(ab))
        # on envoie un signal pour dire qu'il faut tracer les courbes
        self.findMainWindow().simulRequest.emit()

    def renseigneParametres(self, ab):
        """
        Renseigne les paramètres manquants pour l'acide/base ajouté
        """
        ab.c, ab.v = concentrationVolumePrelevement(ab.nom)

    def vide(self):
        """
        Supprime les contenus de la liste
        """
        for r in range(self.count()):
            it = self.takeItem(0)
            del it
        self.contenu = []

    def findMainWindow(self):
        """
        trouve la fenêtre principale
        """
        for t in QApplication.topLevelWidgets():
            if type(t.windowTitle) is type("") and len(t.windowTitle) > 0:
                return t
        return None

    def dropEvent(self, event):
        QListWidget.dropEvent(self, event)
        for r in range(self.count()):
            nom = self.item(r).text()
            for ab in self.listeAcidesBases:
                trouve = False
                if ab.nom == nom:
                    newAb = ab.copie()
                    trouve = True
                    break
            if trouve:
                self.renseigneParametres(newAb)
                self.contenu.append(newAb)
                self.insertItem(r, self.nouveauTexte(newAb))
        # on envoie un signal pour dire qu'il faut tracer les courbes
        QApplication.postEvent(self.findMainWindow(), QEvent(QEvent.Type.User))

    def nouveauTexte(self, ab):
        """
        Le nouveau texte à afficher concernent l'acide/base
        @param ab : l'acide/base à considérer
        """
        return "{nom} {c} mol/L, {v} mL".format(nom=ab.nom, c=ab.c, v=ab.v)

    def chargeNette(self, pH):
        """
        Renvoie la charge électrique des produits mis dans le bécher
        en fonction du pH, compte non tenu des ions H+ et de ce qui vient
        de la burette. UNité du résultat : Faraday
        @param pH, le pH
        """
        charge = 0.0
        for ab in self.contenu:
            charge += ab.chargeNette(pH)
        return charge

    def chargeEau(self, pH):
        """
        renvoie la charge électrique porté par les protons et les ions
        hydroxyde, en fonction du pH
        @param pH le pH
        """
        v = 0.0
        for ab in self.contenu:
            v += ab.v
        return (10**(-pH)-10**(pH-14))*v*1e-3

    def charge(self, pH):
        """
        Renvoie la charge électrique totale, en fonction du pH
        @param pH le pH
        """
        return self.chargeNette(pH) + self.chargeEau(pH)
