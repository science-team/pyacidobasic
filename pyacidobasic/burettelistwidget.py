#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt6.QtWidgets import QInputDialog
from .becherlistwidget import BecherListWidget

licence = """
    file burettelistwidget.py: part of the package pyacidobasic version %s:

    Copyright (C) 2010-2024 Georges Khaznadar <georgesk@debian.org>
    Copyright (C) 2024 philippe DALET <philippe.dalet@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class BuretteListWidget(BecherListWidget):

    def renseigneParametres(self, ab):
        """
        Renseigne les paramètres manquants pour l'acide/base ajouté
        """
        ab.c = QInputDialog.getDouble(self, "burette",
                                      f"Entrez la concentration ({ab.nom})",
                                      0.1, 0.0, 50, 3)[0]

    def vide(self):
        """
        Supprime les contenus de la liste
        et réactive la réception des drops
        """
        BecherListWidget.vide(self)
        self.setAcceptDrops(True)

    def dropEvent(self, event):
        BecherListWidget.dropEvent(self, event)
        # interdit les drops dès qu'on a un produit là.
        if self.count() > 0:
            self.setAcceptDrops(False)

    def nouveauTexte(self, ab):
        """
        Le nouveau texte à afficher concernent l'acide/base
        @param ab : l'acide/base à considérer
        """
        return f"{ab.nom} {ab.c} mol/L"

    def chargeNette(self, pH, v):
        """
        Renvoie la charge nette de ce qui a coulé de la burette, en
        fonction du pH, compte non tenu des ions H+. UNité : le Faraday.
        @param pH le pH
        @param v le volume versé.
        """
        if len(self.contenu) > 0:
            ab = self.contenu[0]
            ab.v = v
            return ab.chargeNette(pH)
        else:
            return 0.0

    def dump(self):
        """
        renvoie un objet à sauvegarder en cas d'écriture dans un fichier
        @return une instance acide-base ou None
        """
        if len(self.contenu) > 0:
            return self.contenu[0]
        else:
            return None

    def load(self, obj):
        """
        Récupère une donnée venant d'un fichier
        @param obj un acide-base ou None
        """
        self.clear()
        self.contenu = []
        if obj is not None:
            self.contenu.append(obj)
            self.addItem(self.nouveauTexte(obj))

    def load_from_json(self, obj):
        """
        Récupère une donnée venant d'un fichier json
        @param obj un objet obtenu par jsonpickle.decode()
        """
        self.clear()
        self.contenu = []
        ab = obj["burette"].contenu
        self.contenu.append(ab[0])
        self.addItem(self.nouveauTexte(ab[0]))

    def chargeEau(self, pH, v):
        """
        renvoie la charge électrique porté par les protons et les ions
        hydroxyde, en fonction du pH et du volume v versé de la burette
        @param pH le pH
        @param v le volume versé.
        """
        return (10**(-pH)-10**(pH-14))*v*1e-3

    def charge(self, pH, v):
        """
        Renvoie la charge électrique totale, en fonction du pH
        et du volume v versé de la burette
        @param pH le pH
        @param v le volume versé.
        """
        return self.chargeNette(pH, v) + self.chargeEau(pH, v)
