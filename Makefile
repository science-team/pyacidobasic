DESTDIR = 
LRELEASE = /usr/lib/qt6/bin/lrelease
PYPACKAGE = pyacidobasic
SHAREDIR = $(DESTDIR)/usr/share/$(PYPACKAGE)
SOURCES = $(shell ls pymecavideo/*.py | grep -v ^Ui_)
UI_SOURCES = $(shell ls pymecavideo/*.ui)
UI_TARGETS = $(patsubst %.ui, Ui_%.py, $(UI_SOURCES))

all:	pyacidobasic.1.gz languages
	make -C pyacidobasic all

pyacidobasic.1.gz: pyacidobasic.1
	gzip -c9 $< > $@

pyacidobasic.1: manpage.xml
	xsltproc --nonet /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/manpages/docbook.xsl manpage.xml

clean:
	make -C pyacidobasic clean
	rm -f *~ *.pyc *.1 *.1.gz lang/*.qm

install: all
	install -d $(SHAREDIR)
	cp -a Pyacidobasic pyacidobasic lang *.acb $(SHAREDIR)
	find $(DESTDIR) -type d -name __pycache__ | xargs rm -rf
	mkdir -p $(DESTDIR)/usr/share/applications
	cp pyacidobasic.desktop $(DESTDIR)/usr/share/applications
	install -d $(DESTDIR)/usr/bin
	install -m 755 pyacidobasic.sh $(DESTDIR)/usr/bin/pyacidobasic
	install -d $(DESTDIR)/usr/share/man/man1
	install -m 644 pyacidobasic.1.gz $(DESTDIR)/usr/share/man/man1
	install -d $(DESTDIR)/usr/share/pixmaps
	install -m 644 pyacidobasic/icons/pyacidobasic.svg $(DESTDIR)/usr/share/pixmaps


languages: tsfiles
	$(LRELEASE) lang/*.ts

tsfiles: 
	cd lang; /usr/lib/qt6/bin/lupdate pyacidobasic.pro

helpfiles:
	@$(MAKE) -C help

.PHONY: clean all install languages helpfiles tsfiles
