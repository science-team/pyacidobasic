from unittest import TestCase
import numpy as np

class MonTest(TestCase):

    def test_arange(self):
        self.assertTrue(np.array_equal(
            np.arange(6), np.array(range(6))))
