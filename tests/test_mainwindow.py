from unittest import TestCase
import numpy as np
import os, PIL.Image

class MonTest(TestCase):

    def test_colors_bleu_bromophenol(self):
        pngFile=os.path.join(
            'pyacidobasic', 'icons', 'bleu-bromophenol.svg.png')
        img=PIL.Image.open(pngFile)
        imgArray=np.frombuffer(img.tobytes(), dtype=np.uint8)
        imgArray=imgArray.reshape((img.size[1], img.size[0], 4))
        # à bas pH, le bleu de bromophenol est jaune
        self.assertTrue(np.array_equal(
            imgArray[44][55][:3], np.array([255, 255, 0])))
        # à pH élevé, le bleu de bromophenol est mauve
        self.assertTrue(np.array_equal(
            imgArray[44][550][:3], np.array([103, 0, 152])))
        return

    def test_ones(self):
        # numpy produit des "uns" facilement
        self.assertTrue(np.array_equal(
            np.ones(2), np.array([1,1])))
        return

    def test_varColor(self):
        r=np.sin(1*np.pi/2)
        v=np.sin(1*np.pi/3)
        b=np.sin(1*np.pi/5)
        self.assertAlmostEqual(r, 1)
        self.assertAlmostEqual(v, 0.86602540378)
        self.assertAlmostEqual(b, 0.58778525229)
        return
    
        
