Pyacidobasic
============

Logiciel interactif permettant la simulation de dosages acide-base

Usage
-----

  * Tirer-glisser un réactif dans le bécher, depuis la liste (par exemple : acide éthanoïque) <br/><a href="img/snap01.jpg" target="illustration"><img alt="Réactif, dans le bécher" width="320" src="img/snap01.jpg"/></a>
  * Tirer-glisser un réactif dans la burette, depuis la liste (par exemple : hydroxyde de sodium) <br/><a href="img/snap02.jpg" target="illustration"><img alt="Réactif, dans la burette" width="320" src="img/snap02.jpg"/></a>
  * Et voilà. <br/><a href="img/snap03.jpg" target="illustration"><img alt="Courbe de titrage" width="320" src="img/snap03.jpg"/></a>
  
Le premier volet (actif par défaut) permet de voir l'évolution du pH en fonction du volume du réactif versé de la burette. Le deuxième volet permet de suivre la totalité des espèces ioniques ou non de la solition.

Dans le deuxième volet, on peut basculer entre deux modes :
  * Concentrations : les concentrations en mol/L sont représentées ; <br/><a href="img/snap04.jpg" target="illustration"><img alt="Mode concentrations" width="320" src="img/snap04.jpg"/></a>
  * Quantités : les quantités en mol sont représentées. <br/><a href="img/snap05.jpg" target="illustration"><img alt="Mode quantités de matière" width="320" src="img/snap04.jpg"/></a>

Développeurs
------------

Les dépendances pour le développement sont :

* qt6-tools-dev-tools,
* qt6-base-dev,
* qt6-l10n-tools,

Les traductions sont mises à jour à l'aide de `/usr/lib/qt6/bin/lrelease`

Le fichier de resource est généré par :
`rcc -g python -o machin_rc.py machin.qrc`

L'édition des fichiers d'interface se fait avec la commande
`/usr/lib/qt6/bin/designer`

