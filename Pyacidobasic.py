#! /usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os

licence = {}
licence['en'] = """
    pyacidobasic version %s:

    a program to simulate acido-basic equilibria

    Copyright (C) 2010-2024 Georges Khaznadar <georgesk@debian.org>
    Copyright (C) 2024 philippe DALET <philippe.dalet@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

licence['fr'] = u"""
    pyacidobasic version %s:

    un programme pour simuler des équilibres acido-basiques

    Copyright (C) 2010-2024 Georges Khaznadar <georgesk@ofset=debian.org>

    Ce projet est un logiciel libre : vous pouvez le redistribuer, le modifier selon
    les termes de la GPL (GNU Public License) dans les termes de la Free Software
    Foundation concernant la version 3 ou plus de la dite licence.

    Ce programme est fait avec l'espoir qu'il sera utile mais SANS AUCUNE GARANTIE.
    Lisez la licence pour plus de détails.

    <http://www.gnu.org/licenses/>.
"""

thispath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, thispath)
sys.path.insert(0, os.path.join(thispath, "pyacidobasic"))

from pyacidobasic import mainwindow

mainwindow.run()
