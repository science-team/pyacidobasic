<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES" sourcelanguage="en_US">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation>Definicion de una muestra</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation>Muestra: </translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation>Concentración (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation>0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation>Volumen (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation>10.0</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="44"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="47"/>
        <source>Click to show/hide the plot</source>
        <translation>Clicar para mostrar/ocultar la curva</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="100"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="103"/>
        <source>Click to change the color of the plot</source>
        <translation>Clicar para cambiar el color de la curva</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation>Laboratorio</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Choose reagents</source>
        <translation>Elección de los reactivos</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="195"/>
        <source>Burette</source>
        <translation>Bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="147"/>
        <location filename="../pyacidobasic/main.ui" line="210"/>
        <location filename="../pyacidobasic/main.ui" line="273"/>
        <location filename="../pyacidobasic/main.ui" line="434"/>
        <location filename="../pyacidobasic/main.ui" line="451"/>
        <location filename="../pyacidobasic/main.ui" line="474"/>
        <location filename="../pyacidobasic/main.ui" line="488"/>
        <location filename="../pyacidobasic/main.ui" line="502"/>
        <location filename="../pyacidobasic/main.ui" line="519"/>
        <location filename="../pyacidobasic/main.ui" line="673"/>
        <location filename="../pyacidobasic/main.ui" line="690"/>
        <location filename="../pyacidobasic/main.ui" line="707"/>
        <location filename="../pyacidobasic/main.ui" line="721"/>
        <location filename="../pyacidobasic/main.ui" line="735"/>
        <location filename="../pyacidobasic/main.ui" line="752"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="261"/>
        <source>Beaker</source>
        <translation>Vaso de precipitados</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="812"/>
        <source>toolBar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation>pyAcidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="161"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation>Elegir un reactivo y arrastrarlo hasta la bureta o el vaso de precipitados</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="164"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation>Lista de reactivos: Arrastrar un reactivo hacia la bureta o el vaso de precipitados.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="204"/>
        <location filename="../pyacidobasic/main.ui" line="207"/>
        <source>drain the burette to fill it again</source>
        <translation>vaciar la bureta para rellenarla de nuevo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="236"/>
        <source>drag a reagent to the burette</source>
        <translation>arrastrar un reactivo hacia la bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="239"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation>Reactivo de la bureta: arrastrar un reactivo desde la lista de arriba.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="267"/>
        <location filename="../pyacidobasic/main.ui" line="270"/>
        <source>drain the beaker to fill it again</source>
        <translation>vaciar el vaso de precipitados y rellenarlo de nuevo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="299"/>
        <source>drag one or more reagents to the beaker</source>
        <translation>arrastrar uno o varios reactivos al vaso de precipitados</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="302"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation>Lista de reactivos del vaso de precipitados: arrastrar reactivos desde la lista de arriba.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="428"/>
        <location filename="../pyacidobasic/main.ui" line="667"/>
        <source>export in PDF format</source>
        <translation>exportar en formato PDF</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="445"/>
        <location filename="../pyacidobasic/main.ui" line="684"/>
        <source>export in JPEG format</source>
        <translation>exportar en formato JPEG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="468"/>
        <location filename="../pyacidobasic/main.ui" line="701"/>
        <source>export in SVG format</source>
        <translation>exporter en formato SVG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="485"/>
        <location filename="../pyacidobasic/main.ui" line="718"/>
        <source>Define the title</source>
        <translation>Definir el título</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="499"/>
        <location filename="../pyacidobasic/main.ui" line="732"/>
        <source>make the abscissa range wider</source>
        <translation>aumentar el rango de las abscisas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="516"/>
        <location filename="../pyacidobasic/main.ui" line="749"/>
        <source>make the abscissa range narrower</source>
        <translation>disminuir el rango de las abscisas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="559"/>
        <source>Curves to plot</source>
        <translation>Curvas a trazar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="785"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="795"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="824"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation>&amp;Abrir... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="832"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation>&amp;Guardar... (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="840"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation>&amp;Guardar como... (Maj Ctrl -S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="845"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation>&amp;Salir (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="853"/>
        <source>&amp;About ...</source>
        <translation>&amp;Acerca de...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="858"/>
        <source>&amp;Manual (F1)</source>
        <translation>&amp;Manual (F1)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="883"/>
        <source>derivative</source>
        <translation>derivada</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="891"/>
        <source>[H3O+]</source>
        <translation>[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="899"/>
        <source>[HO-]</source>
        <translation>[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="904"/>
        <source>Others... (Ctrl-T)</source>
        <translation>Otros... (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="912"/>
        <source>&amp;Examples ...</source>
        <translation>&amp;Ejemplos...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="131"/>
        <source>Filter:</source>
        <translation>Filtro:</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="119"/>
        <source>Type in a few charcters of the reagent</source>
        <translation>Teclear algunas letras del reactivo para seleccionarlo más fácilmente</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="141"/>
        <location filename="../pyacidobasic/main.ui" line="144"/>
        <source>Click to delete the filter</source>
        <translation>Clicar para eliminar el filtro</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="539"/>
        <source>Concentrations/Quantities</source>
        <translation>Concentraciones/Cantidades</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="586"/>
        <source>&amp;Concentrations</source>
        <translation>&amp;Concentraciones</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="596"/>
        <source>&amp;Quantities</source>
        <translation>&amp;Cantidades</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="571"/>
        <location filename="../pyacidobasic/main.ui" line="574"/>
        <source>Choose the type of plot</source>
        <translation>Elegir el tipo de representación</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="116"/>
        <source>Type in a few characters of the reagent</source>
        <translation>Teclear algunos caracteres del reactivo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="340"/>
        <source>Color indicator ...</source>
        <translation>Indicador de pH...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="345"/>
        <source>Bromothymol blue</source>
        <translation>Azul de bromotimol</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="350"/>
        <source>Phenolphtalein</source>
        <translation>Fenolftaleína</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="355"/>
        <source>Helianthin (Methyl orange)</source>
        <translation>Naranja de metilo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="360"/>
        <source>Bromophenol blue</source>
        <translation>Azul de bromofenol</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="365"/>
        <source>Methyl red</source>
        <translation>Rojo de metilo</translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="252"/>
        <location filename="../pyacidobasic/mainwindow.py" line="291"/>
        <source>File to save</source>
        <translation>Fichero a guardar</translation>
    </message>
    <message>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation type="obsolete">Ficheros Acidobasic [*.acb] (*.acb);; Cualquier fichero (*.* *)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="255"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                           All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                               All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="313"/>
        <source>Version error</source>
        <translation>Error de versión</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="317"/>
        <source>This file is not a valid Pyacidobasic file, 
                        version {version}. Error: {err} ; {type(err)}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="330"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <source>Courbe du dosage</source>
        <translation type="obsolete">Curva de valoraci´on</translation>
    </message>
    <message>
        <source>Ce fichier n&apos;est pas un fichier Pyacidobasic valide, de version %1.</source>
        <translation type="obsolete">Este fichero no es un fichero Pyacidobasic valido, versión %1.</translation>
    </message>
    <message>
        <source>Sorry, missing support</source>
        <translation type="obsolete">Lo sentimos, soporte incompleto</translation>
    </message>
    <message>
        <source>The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</source>
        <translation type="obsolete">La version actual de pyqtgraph no admite aún la exportación a PDF. Intente exportar en formato SVG e convierta el archivo posteriormente con un conversor externo</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="76"/>
        <location filename="../pyacidobasic/phplot.py" line="89"/>
        <source>Message about the bug</source>
        <translation>Mensaje sobre el error</translation>
    </message>
    <message>
        <source>Error: %s</source>
        <translation type="obsolete">Error: %s</translation>
    </message>
    <message>
        <source>A bug is possible</source>
        <translation type="obsolete">Un error es posible</translation>
    </message>
    <message>
        <source>This file is not a valid Pyacidobasic file, version %s.</source>
        <translation type="obsolete">Este archivo, no es un archivo Pyacidobasic válido, de versión %s.</translation>
    </message>
    <message>
        <source>A bug is possible, the developer has patched the file ImageExport of the package pyqtgraph, and filed a bug report. As long as it is not fixed, there will be an issue.</source>
        <translation type="obsolete">Un error puede producirse, el programador ha parcheado el archivo ImageExport del paquete pyqtgraph, y enviado una notificación del error. Mientras que éste no sea solucionado, el problema existirá.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="77"/>
        <location filename="../pyacidobasic/phplot.py" line="92"/>
        <source>Error: {e}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
