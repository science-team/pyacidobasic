<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="fr_FR">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation type="unfinished">Concentration (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation type="unfinished">0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation type="unfinished">Volume (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation type="unfinished">10.0</translation>
    </message>
    <message>
        <source>Définition d&apos;un prélèvement</source>
        <translation type="obsolete">Define a sample</translation>
    </message>
    <message>
        <source>Prélèvement : </source>
        <translation type="obsolete">Sample: </translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="44"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="47"/>
        <source>Click to show/hide the plot</source>
        <translation type="unfinished">Click to show/hide the plot</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="100"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="103"/>
        <source>Click to change the color of the plot</source>
        <translation type="unfinished">Click to change the color of the plot</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation type="unfinished">Laboratory</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="195"/>
        <source>Burette</source>
        <translation type="unfinished">Burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="147"/>
        <location filename="../pyacidobasic/main.ui" line="210"/>
        <location filename="../pyacidobasic/main.ui" line="273"/>
        <location filename="../pyacidobasic/main.ui" line="434"/>
        <location filename="../pyacidobasic/main.ui" line="451"/>
        <location filename="../pyacidobasic/main.ui" line="474"/>
        <location filename="../pyacidobasic/main.ui" line="488"/>
        <location filename="../pyacidobasic/main.ui" line="502"/>
        <location filename="../pyacidobasic/main.ui" line="519"/>
        <location filename="../pyacidobasic/main.ui" line="673"/>
        <location filename="../pyacidobasic/main.ui" line="690"/>
        <location filename="../pyacidobasic/main.ui" line="707"/>
        <location filename="../pyacidobasic/main.ui" line="721"/>
        <location filename="../pyacidobasic/main.ui" line="735"/>
        <location filename="../pyacidobasic/main.ui" line="752"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="812"/>
        <source>toolBar</source>
        <translation type="unfinished">toolBar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>pH</source>
        <translation type="unfinished">pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation type="unfinished">pyAcidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="428"/>
        <location filename="../pyacidobasic/main.ui" line="667"/>
        <source>export in PDF format</source>
        <translation type="unfinished">export in PDF format</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="445"/>
        <location filename="../pyacidobasic/main.ui" line="684"/>
        <source>export in JPEG format</source>
        <translation type="unfinished">export in JPEG format</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="468"/>
        <location filename="../pyacidobasic/main.ui" line="701"/>
        <source>export in SVG format</source>
        <translation type="unfinished">export in SVG format</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="499"/>
        <location filename="../pyacidobasic/main.ui" line="732"/>
        <source>make the abscissa range wider</source>
        <translation type="unfinished">make the abscissa range wider</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="516"/>
        <location filename="../pyacidobasic/main.ui" line="749"/>
        <source>make the abscissa range narrower</source>
        <translation type="unfinished">make the abscissa range narrower</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="586"/>
        <source>&amp;Concentrations</source>
        <translation type="unfinished">&amp;Concentrations</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="785"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;File</translation>
    </message>
    <message>
        <source>&amp;Aide</source>
        <translation type="obsolete">&amp;Help</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="824"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation type="unfinished">&amp;Open ... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="832"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation type="unfinished">&amp;Save ... (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="840"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation type="unfinished">Save &amp;as ... (Shift-Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="845"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation type="unfinished">&amp;Quit (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="858"/>
        <source>&amp;Manual (F1)</source>
        <translation type="unfinished">&amp;Manual (F1)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="891"/>
        <source>[H3O+]</source>
        <translation type="unfinished">[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="899"/>
        <source>[HO-]</source>
        <translation type="unfinished">[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="904"/>
        <source>Others... (Ctrl-T)</source>
        <translation type="unfinished">Others... (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="912"/>
        <source>&amp;Examples ...</source>
        <translation type="unfinished">&amp;Examples...</translation>
    </message>
    <message>
        <source>Choix des réactifs</source>
        <translation type="obsolete">Choose reagents</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="161"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation type="unfinished">Choose a reagent and drag it to the burette or the beaker</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="164"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation type="unfinished">List of reagents: drag a reagent to the burette or the beaker.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="204"/>
        <location filename="../pyacidobasic/main.ui" line="207"/>
        <source>drain the burette to fill it again</source>
        <translation type="unfinished">drain the burette to fill it again</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="236"/>
        <source>drag a reagent to the burette</source>
        <translation type="unfinished">drag a reagent to the burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="239"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation type="unfinished">Reagent of the burette: drag a reagent from the above list.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="261"/>
        <source>Beaker</source>
        <translation type="unfinished">Beaker</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="267"/>
        <location filename="../pyacidobasic/main.ui" line="270"/>
        <source>drain the beaker to fill it again</source>
        <translation type="unfinished">drain the beaker to fill it again</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="299"/>
        <source>drag one or more reagents to the beaker</source>
        <translation type="unfinished">drag one or more reagents to the beaker</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="302"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation type="unfinished">List of reagents of the beaker: drag the reagents from the above list.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="485"/>
        <location filename="../pyacidobasic/main.ui" line="718"/>
        <source>Define the title</source>
        <translation type="unfinished">Define the title</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="559"/>
        <source>Curves to plot</source>
        <translation type="unfinished">Curves to plot</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="853"/>
        <source>&amp;About ...</source>
        <translation type="unfinished">&amp;About...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="131"/>
        <source>Filter:</source>
        <translation type="unfinished">Filter:</translation>
    </message>
    <message>
        <source>Taper quelques lettres du réactif pour le sélectionner plus facilement</source>
        <translation type="obsolete">Type in a few charcters of the reagent</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="141"/>
        <location filename="../pyacidobasic/main.ui" line="144"/>
        <source>Click to delete the filter</source>
        <translation type="unfinished">Click to delete the filter</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="539"/>
        <source>Concentrations/Quantities</source>
        <translation type="unfinished">Concentrations/Quantities</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="596"/>
        <source>&amp;Quantities</source>
        <translation type="unfinished">&amp;Quantities</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="571"/>
        <location filename="../pyacidobasic/main.ui" line="574"/>
        <source>Choose the type of plot</source>
        <translation type="unfinished">Choose the type of plot</translation>
    </message>
    <message>
        <source>BBT</source>
        <translation type="obsolete">Bromothymol blue</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="350"/>
        <source>Phenolphtalein</source>
        <translation type="unfinished">Phenolphtalein</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="355"/>
        <source>Helianthin (Methyl orange)</source>
        <translation type="unfinished">Helianthin (Methyl orange)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="360"/>
        <source>Bromophenol blue</source>
        <translation type="unfinished">Bromophenol blue</translation>
    </message>
    <message>
        <source>Rouge de méthyle</source>
        <translation type="obsolete">Methyl red</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="340"/>
        <source>Color indicator ...</source>
        <translation type="unfinished">Color indicator ...</translation>
    </message>
    <message>
        <source>dérivée</source>
        <translation type="obsolete">derivative</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Choose reagents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="116"/>
        <source>Type in a few characters of the reagent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="119"/>
        <source>Type in a few charcters of the reagent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="345"/>
        <source>Bromothymol blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="365"/>
        <source>Methyl red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="795"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="883"/>
        <source>derivative</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <source>Fichier pour enregistrer</source>
        <translation type="obsolete">File to save</translation>
    </message>
    <message>
        <source>Erreur de version</source>
        <translation type="obsolete">Version error</translation>
    </message>
    <message>
        <source>Courbe du dosage</source>
        <translation type="obsolete">Titration plot</translation>
    </message>
    <message>
        <source>Ce fichier n&apos;est pas un fichier Pyacidobasic valide, de version %1.</source>
        <translation type="obsolete">This file is not a valid Pyacidobasic file (version %1).</translation>
    </message>
    <message>
        <source>Sorry, missing support</source>
        <translation type="obsolete">Sorry, missing support</translation>
    </message>
    <message>
        <source>La version courante de pyqtgraph ne supporte pas encore l&apos;export en PDF. Essayez de passer par SVG puis un convertisseur</source>
        <translation type="obsolete">The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</translation>
    </message>
    <message>
        <source>Message relatif au bug</source>
        <translation type="obsolete">Message about the bug</translation>
    </message>
    <message>
        <source>Error: %s</source>
        <translation type="obsolete">Error: %s</translation>
    </message>
    <message>
        <source>A bug is possible</source>
        <translation type="obsolete">A bug is possible</translation>
    </message>
    <message>
        <source>This file is not a valid Pyacidobasic file, version %s.</source>
        <translation type="obsolete">This file is not a valid Pyacidobasic file, version %s.</translation>
    </message>
    <message>
        <source>Un bug est possible, le développeur a patché le fichier ImageExport du paquet pyqtgraph, et posté un rapport de bug. Tant que ce rapport n&apos;est pas traité, il y aura des soucis.</source>
        <translation type="obsolete">A bug is possible, the developer has patched the file ImageExport in the package pyqtgraph, and posted a bug report. As long as this bug is not fixed, there will be an issue.</translation>
    </message>
    <message>
        <source>À propos</source>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="76"/>
        <location filename="../pyacidobasic/phplot.py" line="89"/>
        <source>Message about the bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="252"/>
        <location filename="../pyacidobasic/mainwindow.py" line="291"/>
        <source>File to save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="255"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                           All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                               All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="313"/>
        <source>Version error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="317"/>
        <source>This file is not a valid Pyacidobasic file, 
                        version {version}. Error: {err} ; {type(err)}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="330"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="77"/>
        <location filename="../pyacidobasic/phplot.py" line="92"/>
        <source>Error: {e}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
