<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation>Définition d&apos;un prélèvement</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation>Prélèvement : </translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation>Concentration (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation>0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation>Volume (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation>10.0</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="44"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="47"/>
        <source>Click to show/hide the plot</source>
        <translation>Cliquer pour afficher/cacher la courbe</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="100"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="103"/>
        <source>Click to change the color of the plot</source>
        <translation>cliquer pour changer la couleur de la courbe</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation>Laboratoire</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Choose reagents</source>
        <translation>Choix des réactifs</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="195"/>
        <source>Burette</source>
        <translation>Burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="147"/>
        <location filename="../pyacidobasic/main.ui" line="210"/>
        <location filename="../pyacidobasic/main.ui" line="273"/>
        <location filename="../pyacidobasic/main.ui" line="434"/>
        <location filename="../pyacidobasic/main.ui" line="451"/>
        <location filename="../pyacidobasic/main.ui" line="474"/>
        <location filename="../pyacidobasic/main.ui" line="488"/>
        <location filename="../pyacidobasic/main.ui" line="502"/>
        <location filename="../pyacidobasic/main.ui" line="519"/>
        <location filename="../pyacidobasic/main.ui" line="673"/>
        <location filename="../pyacidobasic/main.ui" line="690"/>
        <location filename="../pyacidobasic/main.ui" line="707"/>
        <location filename="../pyacidobasic/main.ui" line="721"/>
        <location filename="../pyacidobasic/main.ui" line="735"/>
        <location filename="../pyacidobasic/main.ui" line="752"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="261"/>
        <source>Beaker</source>
        <translation>Bécher</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="812"/>
        <source>toolBar</source>
        <translation>barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation>pyAcidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="161"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation>Choisr un réactif et le tirer vers la burette ou le bécher</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="164"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation>Liste des réactifs : Tirer un réactif vers la burette ou vers le bécher.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="204"/>
        <location filename="../pyacidobasic/main.ui" line="207"/>
        <source>drain the burette to fill it again</source>
        <translation>vider la burette pour la remplir à nouveau</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="236"/>
        <source>drag a reagent to the burette</source>
        <translation>tirer un réactif vers la burette</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="239"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation>Réactif de la burette : tirer un réactif depuis la liste ci-dessus.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="267"/>
        <location filename="../pyacidobasic/main.ui" line="270"/>
        <source>drain the beaker to fill it again</source>
        <translation>vider le bécher pour la remplir à nouveau</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="299"/>
        <source>drag one or more reagents to the beaker</source>
        <translation>tirer un ou plusieurs réactifs vers le bécher</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="302"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation>Liste des réactifs du bécher : tirer des réactifs de la liste ci-dessus.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="428"/>
        <location filename="../pyacidobasic/main.ui" line="667"/>
        <source>export in PDF format</source>
        <translation>exporter au format PDF</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="445"/>
        <location filename="../pyacidobasic/main.ui" line="684"/>
        <source>export in JPEG format</source>
        <translation>exporter au format JPEG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="468"/>
        <location filename="../pyacidobasic/main.ui" line="701"/>
        <source>export in SVG format</source>
        <translation>exporter au format SVG</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="485"/>
        <location filename="../pyacidobasic/main.ui" line="718"/>
        <source>Define the title</source>
        <translation>Définir le titre</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="499"/>
        <location filename="../pyacidobasic/main.ui" line="732"/>
        <source>make the abscissa range wider</source>
        <translation>augmenter l&apos;intervalle des abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="516"/>
        <location filename="../pyacidobasic/main.ui" line="749"/>
        <source>make the abscissa range narrower</source>
        <translation>diminuer l&apos;intervalle des abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="559"/>
        <source>Curves to plot</source>
        <translation>Courbes à tracer</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="785"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="795"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="824"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation>&amp;Ouvrir ... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="832"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation>Enregi&amp;strer ... (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="840"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation>&amp;Enregistrer Sous ... (Maj Ctrl -S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="845"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation>&amp;Quitter (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="853"/>
        <source>&amp;About ...</source>
        <translation>À &amp;propos ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="858"/>
        <source>&amp;Manual (F1)</source>
        <translation>&amp;Manuel (F1)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="883"/>
        <source>derivative</source>
        <translation>dérivée</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="891"/>
        <source>[H3O+]</source>
        <translation>[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="899"/>
        <source>[HO-]</source>
        <translation>[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="904"/>
        <source>Others... (Ctrl-T)</source>
        <translation>Autres ... (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="912"/>
        <source>&amp;Examples ...</source>
        <translation>&amp;Exemples ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="131"/>
        <source>Filter:</source>
        <translation>Filtre :</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="119"/>
        <source>Type in a few charcters of the reagent</source>
        <translation>Taper quelques lettres du réactif pour le sélectionner plus facilement</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="141"/>
        <location filename="../pyacidobasic/main.ui" line="144"/>
        <source>Click to delete the filter</source>
        <translation>Cliquer pour effacer le filtre</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="539"/>
        <source>Concentrations/Quantities</source>
        <translation>Concentrations/Quantités</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="586"/>
        <source>&amp;Concentrations</source>
        <translation>&amp;Concentrations</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="596"/>
        <source>&amp;Quantities</source>
        <translation>&amp;Quantités</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="571"/>
        <location filename="../pyacidobasic/main.ui" line="574"/>
        <source>Choose the type of plot</source>
        <translation>Choisir le type de représentation</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="345"/>
        <source>Bromothymol blue</source>
        <translation>BBT</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="350"/>
        <source>Phenolphtalein</source>
        <translation>Phénolphtaléine</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="355"/>
        <source>Helianthin (Methyl orange)</source>
        <translation>Hélianthine</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="360"/>
        <source>Bromophenol blue</source>
        <translation>Bleu de bromophénol</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="365"/>
        <source>Methyl red</source>
        <translation>Rouge de méthyle</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="340"/>
        <source>Color indicator ...</source>
        <translation>Indicateur coloré ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="116"/>
        <source>Type in a few characters of the reagent</source>
        <translation>Taper quelques caractères du réactif</translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <source>Sorry, missing support</source>
        <translation type="obsolete">Désolé, support incomplet</translation>
    </message>
    <message>
        <source>The current version of pyqtgraph does not yet support PDF exports. Try to export SVG then convert it</source>
        <translation type="obsolete">La version courante de pyqtgraph ne supporte pas encore l&apos;export en PDF. Essayez de passer par SVG puis un convertisseur</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="76"/>
        <location filename="../pyacidobasic/phplot.py" line="89"/>
        <source>Message about the bug</source>
        <translation>Message relatif au bug</translation>
    </message>
    <message>
        <source>Error: %s</source>
        <translation type="obsolete">Erreur : %s</translation>
    </message>
    <message>
        <source>A bug is possible</source>
        <translation type="obsolete">Un bug est possible</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="252"/>
        <location filename="../pyacidobasic/mainwindow.py" line="291"/>
        <source>File to save</source>
        <translation>Fichier pour enregistrer</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="255"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                           All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                               All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="317"/>
        <source>This file is not a valid Pyacidobasic file, 
                        version {version}. Error: {err} ; {type(err)}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation type="obsolete">Fichiers Acidobasic [*.acb] (*.acb);; Tous types de fichiers (*.* *)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="313"/>
        <source>Version error</source>
        <translation>Erreur de version</translation>
    </message>
    <message>
        <source>This file is not a valid Pyacidobasic file, version %s.</source>
        <translation type="obsolete">Ce fichier n&apos;est pas un fichier Pyacidobasic valide, de version %s.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="330"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <source>A bug is possible, the developer has patched the file ImageExport of the package pyqtgraph, and filed a bug report. As long as it is not fixed, there will be an issue.</source>
        <translation type="obsolete">Un bug est possible, le développeur a patché le fichier ImageExport du paquet pyqtgraph, et envoyé un rapport de bug. Tant que ce bug ne sera pas réparé, il y aura un problème.</translation>
    </message>
    <message>
        <source>This file is not a valid Pyacidobasic file, version %s. Error: %s ; %s</source>
        <translation type="obsolete">Ce fichier n&apos;est pas un fichier pyAcidobasic valide, version %s. Erreur&#xa0;: %s&#xa0;; %s</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="77"/>
        <location filename="../pyacidobasic/phplot.py" line="92"/>
        <source>Error: {e}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
