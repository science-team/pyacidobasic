SOURCES =  ../pyacidobasic/acidebase.py ../pyacidobasic/becherlistwidget.py ../pyacidobasic/burettelistwidget.py ../pyacidobasic/curveControl.py ../pyacidobasic/__init__.py ../pyacidobasic/phplot.py ../pyacidobasic/prelevement.py ../pyacidobasic/reactifs.py  ../pyacidobasic/version.py ../pyacidobasic/mainwindow.py ../pyacidobasic/doubleplotwidget.py
FORMS =  ../pyacidobasic/curvecontrol.ui ../pyacidobasic/main.ui ../pyacidobasic/prelevement.ui
TRANSLATIONS =  pyacidobasic_en_US.ts pyacidobasic_fr_FR.ts pyacidobasic_es_ES.ts pyacidobasic_ca_CA.ts

CODECFORSRC=UTF-8
#CODEC=UTF-8
