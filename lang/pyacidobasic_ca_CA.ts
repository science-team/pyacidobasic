<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES" sourcelanguage="fr_FR">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="14"/>
        <source>Define a sample</source>
        <translation type="unfinished">Definició d&apos;un assaig</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="45"/>
        <source>Sample: </source>
        <translation type="unfinished">Assaig:</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="61"/>
        <source>Concentration (mol/L)</source>
        <translation type="unfinished">Concentració (mol/L)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="68"/>
        <source>0.1</source>
        <translation type="unfinished">0.1</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="75"/>
        <source>Volume (mL)</source>
        <translation type="unfinished">Volum (mL)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/prelevement.ui" line="82"/>
        <source>10.0</source>
        <translation type="unfinished">10.0</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="44"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="47"/>
        <source>Click to show/hide the plot</source>
        <translation type="unfinished">Clic per mostrar/ocultar la corba</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/curvecontrol.ui" line="100"/>
        <location filename="../pyacidobasic/curvecontrol.ui" line="103"/>
        <source>Click to change the color of the plot</source>
        <translation type="unfinished">clic per canviar el color de la corba</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../pyacidobasic/main.ui" line="74"/>
        <source>Laboratory</source>
        <translation type="unfinished">Laboratori</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="110"/>
        <source>Choose reagents</source>
        <translation type="unfinished">Elecció dels reactius</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="195"/>
        <source>Burette</source>
        <translation type="unfinished">Bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="147"/>
        <location filename="../pyacidobasic/main.ui" line="210"/>
        <location filename="../pyacidobasic/main.ui" line="273"/>
        <location filename="../pyacidobasic/main.ui" line="434"/>
        <location filename="../pyacidobasic/main.ui" line="451"/>
        <location filename="../pyacidobasic/main.ui" line="474"/>
        <location filename="../pyacidobasic/main.ui" line="488"/>
        <location filename="../pyacidobasic/main.ui" line="502"/>
        <location filename="../pyacidobasic/main.ui" line="519"/>
        <location filename="../pyacidobasic/main.ui" line="673"/>
        <location filename="../pyacidobasic/main.ui" line="690"/>
        <location filename="../pyacidobasic/main.ui" line="707"/>
        <location filename="../pyacidobasic/main.ui" line="721"/>
        <location filename="../pyacidobasic/main.ui" line="735"/>
        <location filename="../pyacidobasic/main.ui" line="752"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="261"/>
        <source>Beaker</source>
        <translation type="unfinished">Vas de precipitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="812"/>
        <source>toolBar</source>
        <translation type="unfinished">toolbar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="875"/>
        <source>pH</source>
        <translation type="unfinished">pH</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="26"/>
        <source>pyAcidoBasic</source>
        <translation type="unfinished">pyacidoBasic</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="161"/>
        <source>Choose a reagent and drag it to the burette or the beaker</source>
        <translation type="unfinished">Triar un reactiu i arrastrar-lo cap a la burera o el vas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="164"/>
        <source>List of reagents: drag a reagent to the burette or the beaker.</source>
        <translation type="unfinished">Llista de reactius: Arrossegar un reactiu cap a la bureta o el vas de precipitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="204"/>
        <location filename="../pyacidobasic/main.ui" line="207"/>
        <source>drain the burette to fill it again</source>
        <translation type="unfinished">buidar la bureta per omplir-la de nou</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="236"/>
        <source>drag a reagent to the burette</source>
        <translation type="unfinished">arrossegar un reactiu cap a la bureta</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="239"/>
        <source>Reagent of the burette: drag a reagent from the above list.</source>
        <translation type="unfinished">Reactiu de la bureta: arrossegar un reactiu des de la llista de dalt</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="267"/>
        <location filename="../pyacidobasic/main.ui" line="270"/>
        <source>drain the beaker to fill it again</source>
        <translation type="unfinished">buidar el vas per omplir-lo de nou</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="299"/>
        <source>drag one or more reagents to the beaker</source>
        <translation type="unfinished">arrossegar un o més reactius al vas</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="302"/>
        <source>List of reagents of the beaker: drag the reagents from the above list.</source>
        <translation type="unfinished">Llista de reactius del vas: arrossegar els reactius des de la llista superior.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="428"/>
        <location filename="../pyacidobasic/main.ui" line="667"/>
        <source>export in PDF format</source>
        <translation type="unfinished">exportar a format pdf</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="445"/>
        <location filename="../pyacidobasic/main.ui" line="684"/>
        <source>export in JPEG format</source>
        <translation type="unfinished">exportar a format jpeg</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="468"/>
        <location filename="../pyacidobasic/main.ui" line="701"/>
        <source>export in SVG format</source>
        <translation type="unfinished">exportar a format svg</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="485"/>
        <location filename="../pyacidobasic/main.ui" line="718"/>
        <source>Define the title</source>
        <translation type="unfinished">Definir la concentració</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="499"/>
        <location filename="../pyacidobasic/main.ui" line="732"/>
        <source>make the abscissa range wider</source>
        <translation type="unfinished">augmentar l&apos;interval de les abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="516"/>
        <location filename="../pyacidobasic/main.ui" line="749"/>
        <source>make the abscissa range narrower</source>
        <translation type="unfinished">disminuir l&apos;interval de les abscisses</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="559"/>
        <source>Curves to plot</source>
        <translation type="unfinished">corbes a traçar</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="785"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Fitxer</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="795"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Ajuda</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="824"/>
        <source>&amp;Open ... (Ctrl-O)</source>
        <translation type="unfinished">&amp;Obrir ... (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="832"/>
        <source>&amp;Save ... (Ctrl-S)</source>
        <translation type="unfinished">Guardar ... (Ctrl-S)</translation>
    </message>
    <message>
        <source>Enregistrer Sous ... (Maj Ctrl -S)</source>
        <translation type="obsolete">Guardar com ... (Maj Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="845"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation type="unfinished">Cancel·lar (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="853"/>
        <source>&amp;About ...</source>
        <translation type="unfinished">Sobre ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="858"/>
        <source>&amp;Manual (F1)</source>
        <translation type="unfinished">&amp;Manual (F1)</translation>
    </message>
    <message>
        <source>dérivée</source>
        <translation type="obsolete">derivada</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="891"/>
        <source>[H3O+]</source>
        <translation type="unfinished">[H3O+]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="899"/>
        <source>[HO-]</source>
        <translation type="unfinished">[HO-]</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="904"/>
        <source>Others... (Ctrl-T)</source>
        <translation type="unfinished">Altres ...(Ctrl-T)</translation>
    </message>
    <message>
        <source>Exemples ...</source>
        <translation type="obsolete">Exemples ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="131"/>
        <source>Filter:</source>
        <translation type="unfinished">Filtre :</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="119"/>
        <source>Type in a few charcters of the reagent</source>
        <translation type="unfinished">Drecera: Teclejar algunes lletras del reactiu</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="141"/>
        <location filename="../pyacidobasic/main.ui" line="144"/>
        <source>Click to delete the filter</source>
        <translation type="unfinished">Clicar per eliminar el filtre</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="539"/>
        <source>Concentrations/Quantities</source>
        <translation type="unfinished">Concentracions/Quantitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="586"/>
        <source>&amp;Concentrations</source>
        <translation type="unfinished">Concentracions</translation>
    </message>
    <message>
        <source>Quantités</source>
        <translation type="obsolete">Quantitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="571"/>
        <location filename="../pyacidobasic/main.ui" line="574"/>
        <source>Choose the type of plot</source>
        <translation type="unfinished">Triar el tipus de representació</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="596"/>
        <source>&amp;Quantities</source>
        <translation type="unfinished">Quantitats</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="840"/>
        <source>Save &amp;as ... (Shift Ctrl -S)</source>
        <translation type="unfinished">Guardar com ... (Maj Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="912"/>
        <source>&amp;Examples ...</source>
        <translation type="unfinished">Exemples ...</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="116"/>
        <source>Type in a few characters of the reagent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="340"/>
        <source>Color indicator ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="345"/>
        <source>Bromothymol blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="350"/>
        <source>Phenolphtalein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="355"/>
        <source>Helianthin (Methyl orange)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="360"/>
        <source>Bromophenol blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="365"/>
        <source>Methyl red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/main.ui" line="883"/>
        <source>derivative</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pyacidobasic</name>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="252"/>
        <location filename="../pyacidobasic/mainwindow.py" line="291"/>
        <source>File to save</source>
        <translation type="unfinished">Fitxer a guardar</translation>
    </message>
    <message>
        <source>Acidobasic files [*.acb] (*.acb);; All files (*.* *)</source>
        <translation type="obsolete">Fitxers Acidobasic [*.acb] (*.acb);; Qualsevol fitxer (*.* *)</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="255"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                           All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="294"/>
        <source>Acidobasic files [*.acb] (*.acb);;
                                               All files (*.* *)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="313"/>
        <source>Version error</source>
        <translation type="unfinished">Error de versió</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="317"/>
        <source>This file is not a valid Pyacidobasic file, 
                        version {version}. Error: {err} ; {type(err)}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/mainwindow.py" line="330"/>
        <source>About</source>
        <translation type="unfinished">Sobre</translation>
    </message>
    <message>
        <source>Courbe du dosage</source>
        <translation type="obsolete">Corba de valoració</translation>
    </message>
    <message>
        <source>Ce fichier n&apos;est pas un fichier Pyacidobasic valide, de version %1.</source>
        <translation type="obsolete">Aquest fitxer no es un fitxer Pyacidobasic vàlid, versió %1.</translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="76"/>
        <location filename="../pyacidobasic/phplot.py" line="89"/>
        <source>Message about the bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pyacidobasic/phplot.py" line="77"/>
        <location filename="../pyacidobasic/phplot.py" line="92"/>
        <source>Error: {e}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
